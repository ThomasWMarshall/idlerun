#!/usr/bin/python3
import subprocess
import time, datetime
import sys, os
import signal
import socket, select
from Proc import Proc
from logging import log_message

def get_idle_time (): 
    result = subprocess.run(['xprintidle'], stdout=subprocess.PIPE)
    return int(result.stdout.decode()[:-1])

def handle_SIGCONT(sig, frame):
    print ("Revcieved SIGCONT")
    for proc in procs:
        proc.stop()

def create_socket(name):
    if os.path.exists(name):
        os.remove(name)
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(name)
    return sock

if __name__ == '__main__':
    # Set up SIGCONT handler
    signal.signal(signal.SIGCONT, handle_SIGCONT)

    sock = create_socket("/tmp/idlerun.sock")
    p = select.poll()
    p.register(sock.fileno(), select.POLLIN)

    procs = []

    while True:
        if (len(p.poll(10)) > 0):
            sock.listen(1)
            conn, addr = sock.accept()
            data = conn.recv(1024).split()
            procs.append(Proc(data[1:], int(data[0])))
            
        idle_time = get_idle_time()
        for proc in procs:
            if (idle_time > proc.idle_thresh):
                if proc.started:
                    if proc.cont(): log_message ("Continued {} with pid {}", 'GREEN', proc.command[0].decode(), proc.pid)
                elif proc.start():  log_message ("Started {} with pid {}", 'BLUE', proc.command[0].decode(), proc.pid)
            elif proc.stop():       log_message ("Stopped {} with pid {}", 'FAIL', proc.command[0].decode(), proc.pid)



