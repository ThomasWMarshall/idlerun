#!/usr/bin/python3
import socket, sys

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.connect("/tmp/idlerun.sock")

sock.send((' '.join(sys.argv[1:]).encode('UTF_8')))
