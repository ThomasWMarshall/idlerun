import time
import signal
import subprocess

class Proc:
    def __init__ (self, command, idle_thresh):
        self.command = command
        self.created_time = time.time()
        self.start_time = None
        self.last_stopped = None
        self.last_started = None
        self.started = False
        self.stopped = True
        self.idle_thresh = idle_thresh
        self.process_object = None
        self.pid = None
        self.dead = False

    def start(self):
        self.process_object = subprocess.Popen(self.command)
        self.started = True
        self.stopped = False
        self.pid = self.process_object.pid
        self.start_time = time.time()
        self.last_started = self.start_time
        return True

    def stop(self):
        if self.stopped:
            return False
        self.process_object.send_signal(signal.SIGSTOP)
        self.last_stopped = time.time()
        self.stopped = True
        return True

    def cont(self):
        if not self.stopped:
            return False
        self.process_object.send_signal(signal.SIGCONT)
        self.last_started = time.time()
        self.stopped = False
        return True
