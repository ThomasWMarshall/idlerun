#!/usr/bin/python3
import time, datetime
import sys, os

bcolors = {
        'HEADER': '\033[95m',
        'BLUE': '\033[94m',
        'GREEN': '\033[92m',
        'WARNING': '\033[93m',
        'FAIL': '\033[91m',
        'ENDC': '\033[0m',
        'BOLD': '\033[1m',
        'UNDERLINE': '\033[4m'
        }

def log_message (fstr, color, *args):
    timestamp = "[{}{}{}]{} ".format(bcolors['GREEN'], datetime.datetime.now().strftime('%H:%M:%S'), bcolors['ENDC'], bcolors[color])
    message = fstr.format(*args)
    print (timestamp + message + bcolors['ENDC'])
